//
//  SearchViewController.swift
//  SearchFlickr
//
//  Created by Nazar Starantsov on 16.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    var viewModel: SearchViewModelProtocol {
        didSet {
            self.viewModel.finishedUpdatingCollection = { [weak self] (status) in
                guard let strongSelf = self else { return }
                
                DispatchQueue.main.async {
                    switch status {
                    case .errorWhileLoading:
                        break
                    default:
                        strongSelf.tableView.reloadData()
                    }
                }
            }
            
        }
    }
    
    lazy var inputTextView: UITextView = {
        let input = UITextView()
        input.translatesAutoresizingMaskIntoConstraints = false
        input.font = UIFont.systemFont(ofSize: 18)
        input.layer.cornerRadius = 5
        input.sizeToFit()
        input.backgroundColor = .systemGray2
        
        return input
    }()
  
    lazy var tableView: UITableView = {
        let table = UITableView()
        
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    
    var inputAreaHeight: CGFloat = 40
    
    init() {
        let model = SearchModel(networkManager: NetworkManager())
        let viewModel = SearchViewModel(model: model)
        self.viewModel = viewModel
        // In order for didSet to be called in initializer
        defer {
            self.viewModel = viewModel
        }
        super.init(nibName: nil, bundle: nil)
    }
    
    convenience init(viewModel: SearchViewModel) {
        self.init()
        // In order for didSet to be called in initializer
        defer {
            self.viewModel = viewModel
        }
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addSubviews()
        setupConstraints()
        
        tableView.delegate = self
        tableView.dataSource = self
        inputTextView.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func addSubviews() {
        view.addSubview(inputTextView)
        view.addSubview(tableView)
        view.backgroundColor = .systemBackground
    }
    
    func setupConstraints() {
        inputTextView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        inputTextView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9).isActive = true
        inputTextView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputTextView.heightAnchor.constraint(equalToConstant: inputAreaHeight).isActive = true
        
        tableView.topAnchor.constraint(equalTo: inputTextView.bottomAnchor, constant: 20).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
}


extension SearchViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.viewModel.searchImages(with: textView.text)
    }
}

extension SearchViewController: UITableViewDelegate {}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else { return UITableViewCell()}
        
        let model = viewModel.images[indexPath.row]
        
        cell.textLabel?.text = model.description
        cell.imageView?.image = model.image
        
        return cell
    }
}
