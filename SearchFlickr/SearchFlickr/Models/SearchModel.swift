//
//  SearchModel.swift
//  SearchFlickr
//
//  Created by Nazar Starantsov on 16.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation

protocol SearchModelProtocol {
    var images: [ImageEntity] { get }
    var collectionDidChange: ((CollectionChangeStatus) -> ())? { get set }
    func loadImages(by searchString: String)
    func clearImages()
}

class SearchModel: SearchModelProtocol {
    var images: [ImageEntity] = []
    
    var collectionDidChange: ((CollectionChangeStatus) -> ())?
    var networkManager: NetworkManagerProtocol
    
    init(networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func clearImages() {
        images = []
        collectionDidChange?(.clearImages)
    }
    
    func loadImages(by searchString: String) {
        let url = API.searchPath(text: searchString, extras: "url_m")
        networkManager.getData(at: url) { data in
            guard let data = data else {
                
                self.collectionDidChange?(.errorWhileLoading)
                return
            }
            let responseDictionary = try? JSONSerialization.jsonObject(with: data, options: .init()) as? Dictionary<String, Any>
            
            guard let response = responseDictionary,
                let photosDictionary = response["photos"] as? Dictionary<String, Any>,
                var photosArray = photosDictionary["photo"] as? [[String: Any]] else {
                    self.collectionDidChange?(.errorWhileLoading)
                return
            }

            photosArray = photosArray.count > 10 ? photosArray.suffix(10) : photosArray
            
            let group = DispatchGroup()
            
            var imagesArray: [ImageEntity] = []
            
            for object in photosArray {
                group.enter()
                let urlString = object["url_m"] as? String ?? ""
                let title = object["title"] as? String ?? ""
                
                if let url = URL(string: urlString) {
                    self.networkManager.getData(at: url) {imageData in
                        guard let data = imageData else {
                            group.leave()
                            return
                        }
                        
                        imagesArray.append(ImageEntity(imageData: data, imageDescription: title))
                        group.leave()
                    }
                }
            }
            
            group.notify(queue: DispatchQueue.main) {
                self.images = imagesArray
                self.collectionDidChange?(self.images.count > 0 ? .foundImages : .noImages)
            }
        }
    }
}

enum CollectionChangeStatus {
    case foundImages
    case noImages
    case errorWhileLoading
    case clearImages
}
