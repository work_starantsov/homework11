//
//  ImageLoader.swift
//  SearchFlickr
//
//  Created by Nazar Starantsov on 17.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

class ImageLoader {
    static func loadImage(with urlString: String, completion: @escaping (UIImage?) -> Void) {
        completion(UIImage())
    }
}
