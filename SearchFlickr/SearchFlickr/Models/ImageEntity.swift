//
//  ImageModel.swift
//  SearchFlickr
//
//  Created by Nazar Starantsov on 16.11.2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

import Foundation
import UIKit

struct ImageEntity {
    var imageData: Data
    var imageDescription: String
}

struct ImageViewModelEntity {
    let description: String
    let image: UIImage
}
